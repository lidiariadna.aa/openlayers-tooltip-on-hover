# OpenLayers / Tooltip on Hover

Este ejemplo demuestra como usar un cuadro informativo (_toolpip_) que aparece cuando un usuario pasa el cursor sobre una característica en un mapa de _OL_. Es útil para mostrar datos adicionales de manera intuitiva al explorar el mapa.

Inicialmente, se establece la capa vectorial y se carga el mapa con las características deseadas. Se prepara un elemento HTML que funcionará como el _tooltip_ identificado aquí como `info`. Este elemento es el que mostrará la información de las características sobre las que el usuario pasa el cursor.

El código _JS_ fundamental se centra en detectar el moviviento del cursor sobre el mapa y mostrará la información relevante de las características en el _tooltip_:

```js
let currentFeature;
const displayFeatureInfo = function (pixel, target) {
//Verifica si el cursor está sobre el mapa o sobre una característica.
  const feature = target.closest('.ol-control')
    ? undefined
    : map.forEachFeatureAtPixel(pixel, function (feature) {
        return feature;
      });
//Si hay una característica bajo el cursor, muestra el tooltip con su información
  if (feature) {
    info.style.left = pixel[0] + 'px';
    info.style.top = pixel[1] + 'px';
    if (feature !== currentFeature) {
      info.style.visibility = 'visible';
      info.innerText = feature.get('ECO_NAME');
    }
//Muestra el nombre de la ecoregion
  } else {
    info.style.visibility = 'hidden';
//Oculta el tooltip si no hay una característica
  }
  currentFeature = feature;
};
```
Se usa `forEachFeatureAtPixel` para identificar la característica bajo el cursor.

Para activar la visualización del _tooltip_, se registran varios eventos en el mapa:

```js
map.on('pointermove', function (evt) {
  if (evt.dragging) {
    info.style.visibility = 'hidden';
//Oculta el tooltip mientras se arrastra el mapa
    currentFeature = undefined;
    return;
  }
  const pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel, evt.originalEvent.target);
});

map.on('click', function (evt) {
  displayFeatureInfo(evt.pixel, evt.originalEvent.target);
});
//Tambien muestra el mapa al hacer clic
map.getTargetElement().addEventListener('pointerleave', function () {
  currentFeature = undefined;
  info.style.visibility = 'hidden';
});
//Oculta el tooltip cuando el cursor sale del mapa
```

> En el archivo CSS se puede personalizar el aspecto del _tooltip_. Esto incluye ajustes como el color del texto, el fondo, el tamaño de la fuente, el espaciado y la posición del cursor.

Esto ayuda a obtener información detallada sobre las características del mapa de manera interactiva y dinámica, mejorando la exploración de datos geográficos.

